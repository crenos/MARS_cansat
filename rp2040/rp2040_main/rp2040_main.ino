#include <TinyGPS.h>
#include <SHT3x.h>
//#include <sensirion_arch_config.h>
//#include <sensirion_voc_algorithm.h>
//#include <MPU9250.h>
#include <Adafruit_ADS1X15.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BME280.h>
//#include <Adafruit_LTR390.h>
#include <Adafruit_SGP40.h>
#include <SoftwareSerial.h>
//#include <SD.h>
#include <SPI.h>
#include <Wire.h>

/*      I2C adr       */
#define mpu_adr 0x68
#define ads1_adr 0x48
#define ads2_adr 0x49
#define bme_adr 0x76

/*      PINOUT        */
#define GPS_RX 9u  //0u >  GPS-TX
#define GPS_TX 8u  //1u >  GPS-RX
#define UART_RX 10u
#define UART_TX 11u
#define SD_CS 13u

/*      nastavení     */
#define filename_log "log%02d.csv"
#define SERIAL_BAUD 115200
#define UARTbaud 9600
#define GPS_baud 9600

/*      DEBUG         */
#define DEBUG_EN true
#ifdef DEBUG_EN
#define DEBUG(input) uart.print(input)
#define DEBUGln(input) uart.println(input)
#define DEBUGHEX(input, param) Serial.print(input, param)
#define DEBUGFlush() Serial.flush()
#else
#define DEBUG(input)
#define DEBUGln(input)
#define DEBUGHEX(input, param)
#define DEBUGFlush() ;
#endif
//File dataFile;
Adafruit_ADS1015 ads1;  //i2c
Adafruit_ADS1015 ads2;  //i2c
//MPU9250 mpu;            //i2c
SHT3x Sensor;         //i2c
Adafruit_SGP40 sgp;   //i2c
Adafruit_BME280 bme;  // I2C
TinyGPS gps;          //serial
SoftwareSerial ss(GPS_RX, GPS_TX);
SoftwareSerial uart(UART_RX, UART_TX);
float GPS_lat, GPS_lon, GPS_alt, GPS_speed;
int GPS_HDOP, GPS_sat;
//#define WIRE Wire
void setup() {

  //Serial.begin(SERIAL_BAUD);  //debug

  uart.begin(UARTbaud);  //komunikace uart
  uart.println("");
 // WIRE.begin();
  pinMode(LED_BUILTIN, OUTPUT);
  //pinMode();
/*
  delay(100);
  if (!SD.begin(SD_CS)) {
    DEBUG(" E: Card, ");
  }
  delay(100);
  if (!ads1.begin(ads1_adr)) {
    DEBUG("E: ADS01, ");
  }
  delay(100);
  if (!ads2.begin(ads2_adr)) {
    DEBUG("E: ADS02, ");
  }
  delay(100);
  if (!mpu.setup(mpu_adr)) {
    DEBUG("E: MPU, ");
  }
  delay(100);*/
  if (!bme.begin(bme_adr)) {
    DEBUG("E: bme, ");
  }
  delay(100);
  if (!sgp.begin()) {
    DEBUG("E: sgp40");
    //while (1);
  }
  delay(200);
  digitalWrite(LED_BUILTIN, HIGH);  // turn the LED on (HIGH is the voltage level)
  delay(100);                       // wait for a second
  digitalWrite(LED_BUILTIN, LOW);   // turn the LED off by making the voltage LOW
  delay(500);
  uart.println("I: Rp2040 start.");
  delay(1000);
}
/*            A
    float flat = a     GPS
    float flon = b     GPS
    float falt = c     GPS
    float speed = d    GPS
    int sat = e        GPS
    int HDOP = f       GPS
              B
    float pres = g       bme280
    int humidity =  h    SHT3x
    int humidity2 = i    bme280*
    int temp2 = j        bme280
    k
    temp  = t            SHT3x
              C
    int A = adc0
    int B = adc1
    int C = adc2
    int D = adc3
              D
    int E = adc4
    int F = adc5
    int G = adc6
    int H = adc7
  */
void loop() {
  char sendbuffer[62];
  digitalWrite(LED_BUILTIN, HIGH);  // turn the LED on (HIGH is the voltage level)
  delay(100);                       // wait for a second
  digitalWrite(LED_BUILTIN, LOW);   // turn the LED off by making the voltage LOW
  delay(100);
  //gps A
  uart.print("A_a");
  uart.print(GPS_lat, 6);
  uart.print("_b");
  uart.print(GPS_lon, 6);
  uart.print("_c");
  uart.print(GPS_alt, 1);
  uart.print("_d");
  uart.print(GPS_speed, 1);
  uart.print("_e");
  uart.print(GPS_sat);
  uart.print("_f");
  uart.println(GPS_HDOP);
  //sensors B
  delay(1000);
  Sensor.UpdateData();
  uart.print("B_g");
  uart.print(bme.readPressure());
  uart.print("_h");
  uart.print(Sensor.GetRelHumidity());
  uart.print("_i");
  uart.print(0);
  uart.print("_j");
  uart.print(bme.readTemperature());
  uart.print("_t");
  uart.println(Sensor.GetTemperature());
  //analog C
/*
  if (ads1.begin(ads1_adr)) {
    delay(1000);
    uart.print("C_A");
    uart.print(ads1.readADC_SingleEnded(0));
    uart.print("_B");
    uart.print(ads1.readADC_SingleEnded(1));
    uart.print("_C");
    uart.print(ads1.readADC_SingleEnded(2));
    uart.print("_D");
    uart.println(ads1.readADC_SingleEnded(3));
  }
  //analog D
  if (ads2.begin(ads2_adr)) {
    delay(1000);
    uart.print("D_E");
    uart.print(ads2.readADC_SingleEnded(0));
    uart.print("_F");
    uart.print(ads2.readADC_SingleEnded(1));
    uart.print("_G");
    uart.print(ads2.readADC_SingleEnded(2));
    uart.print("_H");
    uart.println(ads2.readADC_SingleEnded(3));
  }*/
  delay(5000);
}
void setup1() {
  ss.begin(GPS_baud);  //software serial for GPS
}
void loop1() {
  bool newData = false;
  unsigned long chars;
  unsigned short sentences, failed;

  // For one second we parse GPS data and report some key values
  for (unsigned long start = millis(); millis() - start < 1000;) {
    while (ss.available()) {
      char c = ss.read();
      // Serial.write(c); // uncomment this line if you want to see the GPS data flowing
      if (gps.encode(c))  // Did a new valid sentence come in?
        newData = true;
    }
  }

  if (newData) {
    float flat, flon;
    unsigned long age;
    gps.f_get_position(&flat, &flon, &age);
    GPS_lat = (flat == TinyGPS::GPS_INVALID_F_ANGLE ? 0.0 : flat);
    GPS_lon = (flon == TinyGPS::GPS_INVALID_F_ANGLE ? 0.0 : flon);
    GPS_alt = (gps.f_altitude() == TinyGPS::GPS_INVALID_F_ALTITUDE ? 0.0 : gps.f_altitude());
    GPS_speed = (gps.f_speed_kmph());
    GPS_HDOP = (gps.hdop() == TinyGPS::GPS_INVALID_HDOP ? 0 : gps.hdop());
    GPS_sat = (gps.satellites() == TinyGPS::GPS_INVALID_SATELLITES ? 0 : gps.satellites());
    DEBUG("LAT=");
    DEBUG(GPS_lat);
    DEBUG(" LON=");
    DEBUG(GPS_lon);
    DEBUG(" ALT=");
    DEBUG(GPS_alt);
    DEBUG(" Speed=");
    DEBUG(GPS_speed);
    DEBUG(" SAT=");
    DEBUG(GPS_sat);
    DEBUG(" PREC=");
    DEBUG(GPS_HDOP);
  }

  gps.stats(&chars, &sentences, &failed);
  /*DEBUG(" CHARS=");
  DEBUG(chars);
  DEBUG(" SENTENCES=");
  DEBUG(sentences);
  DEBUG(" CSUM ERR=");
  DEBUGln(failed);
  //if (chars == 0)
  //  DEBUGln("** No data GPS");*/
}
