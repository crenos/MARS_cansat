# MARS_cansat


communication format from cansat to station:

#xyyy_
#t26_h20    > temp: 26 hum: 20
"#" = starting character
x = variable
y = data
_ = end of variable

variable:               type                    sensor
a = latitude         (float)                     GPS
b = longitude      (float)                     GPS
c = altitude         (float)                     GPS
d = speed           (float)                     GPS
e = number of satelites (int)             GPS
f = HDOP            (int)                        GPS
g =  pressure      (int)                        bme280
h = humidity       (int)                        SHT3x
i = humidity2      (int)                        bme280
j = temp2           (int)                         bme280
k
l
m
n
o
p
q
r
s
t = temp            (int)                          SHT3x
u
v
w
x
y
z

communication format from station to cansat:


"@xxxx"
"@bce"    > 3.3v on , cam & sensor off

a = 3.3v off
b = 3.3v on
c = camera power off
d = camera power on
e = sensors power off 
f = sensors power on
