#include <RFM69.h>
#include <SPI.h>
#include <SoftwareSerial.h>

#define NETWORKID 122  // Must be the same for all nodes (0 to 255)
#define MYNODEID 2     // My node ID (0 to 255)
#define TONODEID 1     // Destination node ID (0 to 254, 255 = broadcast)
#define FREQUENCY RF69_433MHZ
#define ENCRYPT false             // Set to "true" to use encryption
#define ENCRYPTKEY "T0n3uh0dn3s"  // Use the same 16-byte key on all nodes
#define USEACK false              // Request ACKs or not
#define DEBUG_EN true
#define UARTbaud 9600

/*      PINOUT        */
#define UART_RX 5
#define UART_TX 6
#define RF_CS 10
#define CAM_P 7
#define RPI_P 8
#define SENSOR_P 9
#define RPI_status 4



/*      DEBUG         */
#define DEBUG_EN true
#ifdef DEBUG_EN
#define SERIAL_BAUD 115200
#define DEBUGbegin() Serial.begin(SERIAL_BAUD)
#define DEBUG(input) Serial.print(input)
#define DEBUGln(input) Serial.println(input)
#define DEBUGHEX(input, param) Serial.print(input, param)
#define DEBUGFlush() Serial.flush()
#else
#define DEBUG(input)
#define DEBUGbegin()
#define DEBUGln(input)
#define DEBUGHEX(input, param)
#define DEBUGFlush() ;
#endif

RFM69 radio(RF_CS);                     //spi
SoftwareSerial uart(UART_RX, UART_TX);  //uart
void setup() {

  DEBUGbegin();
  DEBUGln("Starting...");

  uart.begin(UARTbaud);
  radio.initialize(FREQUENCY, MYNODEID, NETWORKID);
  radio.setHighPower();
  //power control
  pinMode(CAM_P, OUTPUT);
  digitalWrite(CAM_P, LOW);
  pinMode(SENSOR_P, OUTPUT);
  digitalWrite(SENSOR_P, LOW);
  pinMode(RPI_P, OUTPUT);
  digitalWrite(RPI_P, LOW);



  if (ENCRYPT) {
    radio.encrypt(ENCRYPTKEY);
  }



  RADIOsend("Cansat(MARS) station started", 20);
}

void loop() {
  //DEBUGln("ok");
  static char sendbuffer[62];
  static int sendlength = 0;
  // SENDING
  if (uart.available() > 0) {
    char input = uart.read();

    if (input != '\r')  // not a carriage return
    {
      sendbuffer[sendlength] = input;
      sendlength++;
    }

    // If the input is a carriage return, or the buffer is full:

    if ((input == '\r') || (sendlength == 61))  // CR or buffer full
    {
      // Send the packet!
      RADIOsend(sendbuffer, sendlength);
      sendlength = 0;  // reset the packet
    }
  }

  if (Serial.available() > 0) {
    char input = Serial.read();
    if (input != '\r')  // not a carriage return
    {
      sendbuffer[sendlength] = input;
      sendlength++;
    }
    if ((input == '\r') || (sendlength == 61))  // CR or buffer full
    {
      controll(sendbuffer);
      RADIOsend(sendbuffer, sendlength);
      sendlength = 0;  // reset the packet
    }
  }



  // RECEIVING
  if (radio.receiveDone())  // Got one!
  {
    // Print out the information:

    DEBUG("received from node ");
    DEBUG(radio.SENDERID);
    DEBUG(": [");

    // The actual message is contained in the DATA array,
    // and is DATALEN bytes in size:

    for (byte i = 0; i < radio.DATALEN; i++) {
      DEBUG((char)radio.DATA[i]);
    }

    controll(sendbuffer);

    DEBUG("], RSSI ");
    DEBUGln(radio.RSSI);
    controll(radio.DATA);

    if (radio.ACKRequested()) {
      radio.sendACK();
      DEBUGln("ACK sent");
    }
  }
}

//void DEBUG(,int level){}
void RADIOsend(const char sendbuffer[62], int sendlength) {
  //static int sendlength = sizeof(sendbuffer);

#ifdef DEBUG_EN
  DEBUG("sending to node ");
  DEBUG(TONODEID);
  DEBUG(": [");
  for (byte i = 0; i < sendlength; i++) {
    DEBUG(sendbuffer[i]);
  }
  DEBUGln("]");
#endif

  if (USEACK) {
    if (radio.sendWithRetry(TONODEID, sendbuffer, sendlength))
      DEBUG("ACK received!");
    else
      DEBUG("no ACK received :(");
  } else  // don't use ACK
  {
    DEBUG("Sending without ACK");
    radio.send(TONODEID, sendbuffer, sendlength);
  }
  DEBUGln("    Send ok");
}
void controll(char data[62]) {
  if (data[0] == '@') {
    for (byte i = 1; i < strlen(data); i++) {
      switch (data[i]) {
        case 'a':
          digitalWrite(RPI_P, LOW);
          break;
        case 'b':
          digitalWrite(RPI_P, HIGH);
          break;
        case 'c':
          digitalWrite(CAM_P, LOW);
          break;
        case 'd':
          digitalWrite(CAM_P, HIGH);
          break;
        case 'e':
          digitalWrite(SENSOR_P, LOW);
          break;
        case 'f':
          digitalWrite(SENSOR_P, HIGH);
          break;
      }
    }
  }
}