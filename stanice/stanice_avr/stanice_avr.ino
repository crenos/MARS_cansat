



/*      DEBUG         */
#define DEBUG_EN

#ifdef DEBUG_EN
#define SERIAL_BAUD 115200
#define DEBUG(input) Serial.print(input)
#define DEBUGln(input) Serial.println(input)
#define DEBUGHEX(input, param) Serial.print(input, param)
#define DEBUGFlush() Serial.flush()
#else
#define DEBUG(input)
#define DEBUGln(input)
#define DEBUGHEX(input, param)
#define DEBUGFlush() ;
#endif

#include <RFM69.h>
#include <SPI.h>
#include "Adafruit_GFX.h"
#include "Adafruit_GC9A01A.h"


#define NETWORKID 122  // Must be the same for all nodes (0 to 255)
#define MYNODEID 1     // My node ID (0 to 255)
#define TONODEID 255   // Destination node ID (0 to 254, 255 = broadcast)
#define FREQUENCY RF69_433MHZ
#define ENCRYPT false                  // Set to "true" to use encryption
#define ENCRYPTKEY "TOPSECRETPASSWRD"  // Use the same 16-byte key on all nodes
#define USEACK false  // Request ACKs or not


/*      PINOUT        */
#define TFT_DC 8
#define TFT_CS 9

RFM69 radio;
Adafruit_GC9A01A tft(TFT_CS, TFT_DC);
int temp, humidity, speed, age, alt2, humidity2, temp2, pressure, sat, HDOP;
float flat, flon, falt, pres;

void setup() {
  tft.begin();
  tft.setRotation(4);
  tft.setCursor(30, 30);
  tft.fillScreen(GC9A01A_BLACK);
  tft.setTextColor(GC9A01A_WHITE);
  tft.setTextSize(3);
  tft.print(" Node: ");
  tft.setTextColor(GC9A01A_YELLOW);
  tft.setTextSize(3);
  tft.println(MYNODEID);

  tft.setTextColor(GC9A01A_GREEN);
  tft.setTextSize(5);
  tft.setCursor(50, 100);
  tft.print("ready");
  Serial.begin(SERIAL_BAUD);
  Serial.print("Node ");
  Serial.print(MYNODEID, DEC);
  Serial.println(" ready");

  // Initialize the RFM69HCW:

  radio.initialize(FREQUENCY, MYNODEID, NETWORKID);
  radio.setHighPower();  // Always use this for RFM69HCW

  // Turn on encryption if desired:

  if (ENCRYPT)
    radio.encrypt(ENCRYPTKEY);
  delay(1000);
}

void loop() {
  static char sendbuffer[62];
  static char data[62];
  static int sendlength = 0;


  // SENDING
  if (Serial.available() > 0) {
    char input = Serial.read();

    if (input != '\r')  // not a carriage return
    {
      sendbuffer[sendlength] = input;
      sendlength++;
    }
    if ((input == '\r') || (sendlength == 61))  // CR or buffer full
    {
      Serial.print("sending to node ");
      Serial.print(TONODEID, DEC);
      Serial.print(": [");
      for (byte i = 0; i < sendlength; i++)
        Serial.print(sendbuffer[i]);
      Serial.println("]");
      if (USEACK) {
        if (radio.sendWithRetry(TONODEID, sendbuffer, sendlength))
          Serial.println("ACK received!");
        else
          Serial.println("no ACK received :(");
      }
      else  // don't use ACK
      {
        radio.send(TONODEID, sendbuffer, sendlength);
      }

      sendlength = 0;  // reset the packet
    }
  }

  // RECEIVING
  if (radio.receiveDone())  // Got one!
  {
    // Print out the information:
    tft.fillScreen(GC9A01A_BLACK);
    tft.setTextColor(GC9A01A_BLUE);
    tft.setTextSize(2);
    tft.setCursor(30, 30);
    tft.println(" Data received.");

    Serial.print("received from node ");
    Serial.print(radio.SENDERID, DEC);
    Serial.print(": [");
    for (byte i = 0; i < radio.DATALEN; i++) {

      Serial.print((char)radio.DATA[i]);
    }
    // RSSI is the "Receive Signal Strength Indicator",
    // smaller numbers mean higher power.

    Serial.print("], RSSI ");
    Serial.println(radio.RSSI);

    //tft.setCursor(7, 0);
    tft.print("  RSSI ");
    tft.println(radio.RSSI);


    // Send an ACK if requested.
    // (You don't need this code if you're not using ACKs.)

    if (radio.ACKRequested()) {
      radio.sendACK();
      Serial.println("ACK sent");
    }
    //decode_data();
    //if (radio.DATA[0] == '#') {
    getdata();
    delay(100);
    //tft.setCursor(30,);
    //lcd.clear();
    //lcd.setCursor(0, 0);

    //lcd.setCursor(0, 1);
    tft.print("  Lat: ");
    tft.print(flat,6);
    tft.println("   ");
    Serial.print("Lat: ");
    Serial.println(flat);

    tft.print("  Lon: ");
    tft.print(flon,6);
    tft.println("   ");
    Serial.print("lon: ");
    Serial.println(flon);

    tft.print("  Alt: ");
    tft.print(falt);
    tft.println("   ");
    Serial.print("Alt: ");
    Serial.println(falt);

    tft.print("  Speed: ");
    tft.print(speed);
    tft.println("   ");
    Serial.print("Speed: ");
    Serial.println(speed);

    tft.print("  Temp: ");
    tft.print(temp);
    tft.println("   ");
    Serial.print("Temp: ");
    Serial.println(temp);
    //lcd.setCursor(0, 7);
    tft.print("  Hum: ");
    tft.print(humidity);
    tft.println("   ");
    Serial.print("Hum: ");
    Serial.println(humidity);
    //}
  }
} /*
    float flat = a     GPS
    float flon = b     GPS
    float falt = c     GPS
    float speed = d    GPS
    int sat = e        GPS
    int HDOP = f       GPS
    float pres = g       bme280
    int humidity =  h    SHT3x
    int humidity2 = i    bme280*
    int temp2 = j        bme280
    k
    temp  = t            SHT3x
    int A = adc0
    int B = adc1
    int C = adc2
    int D = adc3
    int E = adc4
    int F = adc5
    int G = adc6
    int H = adc7*/
int getdata() {
  if (radio.DATA[0] == '#') {
    //DEBUG("getdata: ");
    for (byte i = 1; i < radio.DATALEN; i++) {
      DEBUG((char)radio.DATA[i]);
      switch (radio.DATA[i]) {
        case 'a':
          flat = (getfloat(i));
          break;
        case 'b':
          flon = (getfloat(i));
          break;
        case 'c':
          falt = (getfloat(i));
          break;
        case 'd':
          speed = (getfloat(i));
          break;
        case 'e':
          sat = (getint(i));
          break;
        case 'f':
          HDOP = (getint(i));
          break;
        case 't':
          temp = (getint(i));
          break;
        case 'h':
          humidity = (getint(i));
          break;
      }
    }
    DEBUGln("");
  }
  return (0);
}
int getint(int i) {
  int y = 0;
  char out[10];
  i++;
  while ((radio.DATA[i] != '_') && !(i >= radio.DATALEN)) {
    out[y] = radio.DATA[i];
    y++;
    i++;
  }
  return (atoi(out));
}
float getfloat(int i) {
  int y = 0;
  char out[10];
  i++;
  while ((radio.DATA[i] != '_') && !(i >= radio.DATALEN)) {
    //DEBUG(radio.DATA[i]);
    out[y] = radio.DATA[i];
    y++;
    i++;
  }
  return (atof(out));
}